# D3RPC
Diablo 3 Discord Rich Presence

Quick and dirty way to get Discord's Rich Presence working with Diablo 3. You have to set everything up manually for now.
APIs are a hassle. Feel free to fork though.

**Installation**

>Just get the executable for either Windows or Linux in the releases tab.

you need python3 installed ~~until I compile this into an executable~~

*modules*
>- pip install tkinter
>- pip install pypresence

*download *
the main branch source code and extract it to some folder

*running it*
open terminal in that folder, or cd into it and run python3 ./d3rpc.py

Feel free to add me on Discord or DM me here.
SailorZoop#9082

**Things to add**
>- always on top button
>- figure out how to pack the png background in executables
>- get actual blizzard's API working
>- connection status indicator


Thanks to Commando950 for helping me with the dictionary and connection check.

![image](https://user-images.githubusercontent.com/26927890/116476301-524aa700-a87b-11eb-9e22-88e109492bda.png)

![image](https://user-images.githubusercontent.com/26927890/116476391-73ab9300-a87b-11eb-8a05-58c26aa7670f.png)

![d3rpcscreenshot](https://user-images.githubusercontent.com/26927890/116476193-29c2ad00-a87b-11eb-99f3-6f8a860e4c30.png)

